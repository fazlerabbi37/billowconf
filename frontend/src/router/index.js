import Vue from 'vue';
import VueRouter from 'vue-router';
import Root from '../views/Root.vue';
import Home from '../views/Home.vue';
import SignIn from '../views/SignIn.vue';
import Logout from '../views/Logout.vue';
import PasswordResetRequest from '../views/PasswordResetRequest.vue';
import PasswordResetSent from '../views/PasswordResetSent.vue';
import PasswordReset from '../views/PasswordReset.vue';
import Administration from '../views/Admin.vue';
import AdminDashboard from '../views/AdminDashboard.vue';
import ConferenceAdmin from '../views/ConferenceAdmin.vue';
import ConferenceManageAdmin from '../views/ConferenceManageAdmin.vue';
import TalkAdmin from '../views/TalkAdmin.vue';
import TalkManageAdmin from '../views/TalkManageAdmin.vue';
import RoomAdmin from '../views/RoomAdmin.vue';
import RoomManageAdmin from '../views/RoomManageAdmin.vue';
import UserAdmin from '../views/UserAdmin.vue';
import UserManageAdmin from '../views/UserManageAdmin.vue';
import ImportSchedule from '../views/ImportSchedule.vue';
import store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Root,
    children: [
      {
        path: '',
        name: 'Home',
        component: Home,
      },
      {
        path: '/accounts/login/',
        name: 'SignIn',
        component: SignIn,
        props: (route) => ({ next: route.query.next }),
      },
      {
        path: '/accounts/logout/',
        name: 'Logout',
        component: Logout,
      },
      {
        path: '/accounts/reset/request/',
        name: 'PasswordResetRequest',
        component: PasswordResetRequest,
      },
      {
        path: '/accounts/reset/sent/',
        name: 'PasswordResetSent',
        component: PasswordResetSent,
      },
      {
        path: '/accounts/reset/:token/',
        name: 'PasswordReset',
        component: PasswordReset,
        props: true,
      },
    ]
  },
  {
    path: '/admin/',
    component: Administration,
    meta: { requiresAuth: true },
    children: [
      {
        path: '',
        component: AdminDashboard,
        name: 'AdminDashboard',
      },
      {
        path: 'conference/',
        component: ConferenceAdmin,
        name: 'ConferenceAdmin',
      },
      {
        path: 'conferences/import-schedule/',
        component: ImportSchedule,
        name: 'ImportSchedule',
      },
      {
        path: 'conferences/create/',
        component: ConferenceManageAdmin,
        name: 'ConferenceCreateAdmin',
      },
      {
        path: 'conferences/:sid/edit/',
        component: ConferenceManageAdmin,
        name: 'ConferenceEditAdmin',
        props: true,
      },
      {
        path: 'talks/',
        component: TalkAdmin,
        name: 'TalkAdmin',
      },
      {
        path: 'talks/create/',
        component: TalkManageAdmin,
        name: 'TalkCreateAdmin',
      },
      {
        path: 'talks/:sid/edit/',
        component: TalkManageAdmin,
        name: 'TalkEditAdmin',
        props: true,
      },
      {
        path: 'rooms/',
        component: RoomAdmin,
        name: 'RoomAdmin',
      },
      {
        path: 'rooms/create/',
        component: RoomManageAdmin,
        name: 'RoomCreateAdmin',
      },
      {
        path: 'rooms/:sid/edit/',
        component: RoomManageAdmin,
        name: 'RoomEditAdmin',
        props: true,
      },
      {
        path: 'users/',
        component: UserAdmin,
        name: 'UserAdmin',
      },
      {
        path: 'users/create/',
        component: UserManageAdmin,
        name: 'UserCreateAdmin',
      },
      {
        path: 'users/:sid/edit/',
        component: UserManageAdmin,
        name: 'UserEditAdmin',
        props: true,
      },
    ]
  },
];

const router = new VueRouter({
  mode: 'history',
  base: '',
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isAuthenticated) {
      next({
        name: 'SignIn',
        query: { next: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
