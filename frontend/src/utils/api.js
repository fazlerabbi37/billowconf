import axios from 'axios';

/**
 * Manages the interaction with the backend API
 */
export default class Api {
  constructor() {
  }

  conference(id) {
    return axios.get(`/api/conferences/${id}/`);
  }

  conferences(filter) {
    let url = '/api/conferences/';
    if (filter) {
      url = `${url}/?${filter}`;
    }
    return axios.get(url);
  }

  webrtcConfiguration() {
    return axios.get('/api/conferences/webrtc/');
  }

  currentConference() {
    return axios.get('/api/conferences/current/');
  }

  editConference(id, name, irc, start, end) {
    return axios.put(`/api/conferences/${id}/`, {
      id: id,
      name: name,
      irc_server: irc,
      start_date: start,
      end_date: end
    });
  }

  createConference(name, irc, start, end) {
    return axios.post('/api/conferences/', {
      name: name,
      irc_server: irc,
      start_date: start,
      end_date: end
    });
  }

  uploadSchedule(conference, formData) {
    return axios.post(`/api/conferences/${conference}/schedule/`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  room(id) {
    return axios.get(`/api/rooms/${id}/`);
  }

  rooms(filter) {
    let url = '/api/rooms/';
    if (filter) {
      url = `${url}?${filter}`;
    }
    return axios.get(url);
  }

  editRoom(id, name, conference, ircChannel) {
    return axios.put(`/api/rooms/${id}/`, {
      id: id,
      name: name,
      conference: conference,
      irc_channel: ircChannel,
    });
  }

  createRoom(name, conference, ircChannel) {
    return axios.post('/api/rooms/', {
      name: name,
      conference: conference,
      irc_channel: ircChannel,
    });
  }

  deleteRoom(id) {
    return axios.delete(`/api/rooms/${id}/`);
  }

  talks(filter) {
    let url = '/api/talks/';
    if (filter) {
      url = `${url}?${filter}`;
    }
    return axios.get(url);
  }

  talk(id) {
    return axios.get(`/api/talks/${id}/`);
  }

  editTalk(id, formData) {
    return axios.put(`/api/talks/${id}/`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  createTalk(formData) {
    return axios.post(`/api/talks/`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  deleteTalk(id) {
    return axios.delete(`/api/talks/${id}/`);
  }

  login(user) {
    return axios.post('/api/auth/token/', user);
  }

  users(filter) {
    let url = '/api/users/';
    if (filter) {
      url = `${url}?${filter}`;
    }
    return axios.get(url);
  }

  user(id) {
    return axios.get(`/api/users/${id}/`);
  }

  editUser(id, name, email, isStaff) {
    return axios.put(`/api/users/${id}/`, {
      id: id,
      name: name,
      email: email,
      is_staff: isStaff,
    });
  }

  createUser(name, email, isStaff) {
    return axios.post('/api/users/', {
      name: name,
      email: email,
      is_staff: isStaff,
    });
  }

  deleteUser(id) {
    return axios.delete(`/api/users/${id}/`);
  }

  currentUser() {
    return axios.get('/api/users/current/');
  }

  conferenceManagementPeople(filter) {
    let url = '/api/conferencemanagementpeople/';
    if (filter) {
      url = `${url}?${filter}`;
    }
    return axios.get(url);
  }

  requestResetPassword(email) {
    return axios.post('/api/password/reset/request/', {
      email: email
    });
  }

  validateResetToken(token) {
    return axios.post('/api/password/reset/validate/', {
      token: token
    });
  }

  resetPassword(token, password) {
    return axios.post('/api/password/reset/confirm/', {
      token: token,
      password: password,
    });
  }
}
