import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';
import Api from '@/utils/api.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    conference: {
      id: '',
      name: 'BillowConf',
      url: ''
    },
    webrtcServer: '',
    webrtcRoot: '',
    webrtcWsRoot: '',
    webrtcIceServers: [],
    rooms: [],
    token: localStorage.getItem('user-token') || '',
    status: '',
    user: () => {
      const user = localStorage.getItem('user');
      if (user && user !== '') {
        return JSON.parse(user);
      }
      return '';
    },
  },
  getters: {
    authStatus: state => state.status,
    isAuthenticated: state => !!state.token,
    isStaff: state => state.user().is_staff,
    displayName: state => {
      const user = state.user()
      if (user === '') return '';
      return user.username;
    },
  },
  mutations: {
    conference(state, conference) {
      state.conference = conference;
      var conf = conference.id;
      if (conf != '') {
        var api = new Api();
        api.rooms(`conference=${conf}`).then(response => {
          state.rooms = response.data.results;
        }).catch(error => {
          console.error(error);
        });
        api.webrtcConfiguration().then(response => {
          if (response.data.janus_url) {
            state.webrtcServer = response.data.janus_url;
            state.webrtcRoot = response.data.janus_root;
            state.webrtcWsRoot = response.data.janus_ws_root;
            state.webrtcIceServers = response.data.ice_servers;
          } else {
            state.webrtcServer = response.data.jitsi_url;
          }
        }).catch(error => {
          state.webrtcServer = '';
          state.webrtcIceServers = [];
          console.error(error);
        });
      }
    },
    'auth-request': (state) => {
      state.status = 'loading';
    },
    'auth-success': (state, token) => {
      state.status = 'auth success';
      state.token = token;
    },
    'auth-error': (state) => {
      state.status = 'error';
    },
    'auth-logout': (state) => {
      state.status = '';
      state.token = '';
    },
    'user-request': (state) => {
      state.status = 'fetching user';
    },
    'user-success': (state) => {
      state.status = 'success';
    },
  },
  actions: {
    'auth-request': ({commit, dispatch}, user) => {
      return new Promise((resolve, reject) => {
        commit('auth-request');
        var api = new Api();
        api.login(user).then(response => {
          const token = response.data.token;
          localStorage.setItem('user-token', token);
          axios.defaults.headers.common['Authorization'] = `Token ${token}`;
          commit('auth-success', token);
          dispatch('user-request');
          resolve(response);
        }).catch(error => {
          commit('auth-error', error);
          console.error(error);
          localStorage.removeItem('user-token');
          reject(error);
        });
      });
    },
    'auth-logout': ({commit}) => {
      return new Promise(resolve => {
        commit('auth-logout');
        localStorage.removeItem('user-token');
        localStorage.removeItem('user');
        delete axios.defaults.headers.common['Authorization'];
        resolve();
      });
    },
    'user-request': ({commit}) => {
      return new Promise((resolve, reject) => {
        commit('user-request');
        var api = new Api();
        api.currentUser().then(response => {
          localStorage.setItem('user', JSON.stringify(response.data));
          commit('user-success');
          resolve(response);
        }).catch(error => {
          commit('auth-error');
          console.error(error);
          localStorage.removeItem('user');
          localStorage.removeItem('user-token');
          reject(error);
        });
      });
    }
  },
  modules: {}
});
