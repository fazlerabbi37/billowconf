###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.core.management.base import BaseCommand
from billowconf.models import Room
from billowconf.tasks import run_room_stream

class Command(BaseCommand):
    help = 'Schedules all rooms to stream to their WebRTC rooms'

    def handle(self, *args, **options):
        for room in Room.objects.exclude(webrtc_channel=-1):
            run_room_stream.delay(room.id)
