###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import asyncio
from celery import shared_task
from .utils import create_webrtc_room, stream_webrtc_room
from .models import Room

@shared_task
def create_room(pk):
    instance = Room.objects.filter(pk=pk)
    if not instance.exists():
        # Instance has been deleted, there is nothing to do
        return
    instance = instance.get()
    if instance.webrtc_channel == -1:
        instance.webrtc_channel = create_webrtc_room(instance.name)
        instance.save()

@shared_task(bind=True)
def run_room_stream(self, pk):
    instance = Room.objects.filter(pk=pk)
    if not instance.exists():
        # Instance has been deleted, there is nothing to do
        return
    instance = instance.get()
    stream_webrtc_room(instance)
