###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.conf import settings
from django.urls import path, include, re_path
from django.views.generic import TemplateView
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from . import views

router = routers.DefaultRouter()
router.register(r'groups', views.GroupViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'rooms', views.RoomViewSet)
router.register(r'talks', views.TalkViewSet)
router.register(r'conferences', views.ConferenceViewSet)
router.register(r'conferencemanagementpeople', views.ConferenceManagementPersonViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('api/password/reset/request/', views.reset_password_request),
    path('api/password/reset/validate/', views.validate_password_reset_token),
    path('api/password/reset/confirm/', views.reset_password_confirm),
    path('api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/auth/token/', obtain_auth_token),
    re_path('^.*$', views.index, name='index'),
]
