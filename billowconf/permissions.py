###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from rest_framework import permissions

class ReadOnly(permissions.BasePermission):
    """
    Only allow read access to resource
    """
    def has_permissions(self, request, view):
        return request.method in permissions.SAFE_METHODS

class IsAdminOrIsSelf(permissions.BasePermission):
    """
    Only allow staff to access the users and let the current user access themselves
    """
    def has_object_permissions(self, request, view, obj):
        if request.user:
            return request.user.is_staff or obj == request.user
        return False

class IsAdminOrReadOnly(permissions.BasePermission):
    """
    Staff are allowed to edit the object, while everyone else is allowed
    read-only access
    """
    def has_permission(self, request, view):
        return bool(
            request.method in permissions.SAFE_METHODS or
            request.user and
            request.user.is_staff
        )

class IsAdminOrAuthenticatedReadOnly(permissions.BasePermission):
    """
    Staff are allowed to edit the object, while authenticated users are allowed
    read-only access
    """
    def has_permission(self, request, view):
        return bool(
            (request.method in permissions.SAFE_METHODS and
            request.user.is_authenticated) or
            (request.user and
            request.user.is_staff)
        )
