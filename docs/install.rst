Install
=======

There are several components to a running instance of BillowConf:

* Janus or Jitsi WebRTC Server
* API Server
* Front end client
* STUN/TURN Servers (optional)

For setting up a basic instance, Jitsi can be used for WebRTC. This simplifies
setup, but limits the functionality of the system, as it does not allow
restriction on who is talking, who has video, etc.

BillowConf uses Google public STUN servers by default, but these can be replaced
in the API server configuration. TURN servers are used as a fail-safe when the
STUN protocol is unable to traverse the NAT setup between two clients. There are
no TURN servers configured by default, and administrators will have to host
their own if they wish to use any.

WebRTC Server
-------------

BillowConf supports using either Jitsi Meet or Janus as the WebRTC server. Jitsi
can be used without any additional setup, but it lacks many of the features that
BillowConf supports, such as automatic presenter feed displays and moderation.
BillowConf can use the Jitsi Meet instance `provided by upstream`_ or a custom
instance.  Using Janus requires setting up a Janus server and configuring
BillowConf to use it. Follow `upstream's install guide`_ to set up a working
Janus server.

.. _provided by upstream: https://meet.jit.si
.. _upstream's install guide: https://janus.conf.meetecho.com/docs/deploy.html

Deploying BillowConf
--------------------
BillowConf is a Python web app, using Django with a JavaScript (Vue) front-end.
Building the front-end requires ``npm``. This is then used to build the
front-end into a module that can be served by Django::

    sudo apt install npm git
    git clone https://gitlab.com/billowconf/billowconf.git
    cd billowconf/frontend
    npm install
    npm run build

Once the front-end is built, the Django server can be configured and run using
uWSGI. This guide assumes that the Django server is being deployed behind an
Nginx reverse proxy with SSL/TLS support, but any web server will work. SSL/TLS
support can be removed by changing the web server configuration.

Django and BillowConf required dependencies are installed as follows::

    sudo apt install libxml2-dev libxslt-dev libjs-janus libjs-webrtc-adapter \
      rabbitmq-server libavdevice-dev libavfilter-dev libopus-dev libvpx-dev \
      libsrtp2-dev pkg-config nginx mariadb-server uwsgi uwsgi-plugin-python3 \
      python3 python3-pip libmariadb-dev-compat libmariadb-dev

Configuring BillowConf is done by creating the ``billowconf/local_settings.py``
with at least the following settings::

    from billowconf.settings import *
    SECRET_KEY = '$your_secret_key'
    HOST_DOMAIN = '$your_billowconf_url'
    DEBUG = False
    ALLOWED_HOSTS = ['*']
    LANGUAGE_CODE = '$your_language_code'
    TIME_ZONE = '$your_time_zone'
    EMAIL_BACKEND = 'djangocode.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = '$your_smtp_host'
    EMAIL_PORT = $your_smtp_port
    EMAIL_HOST_USER = '$your_smtp_user'
    EMAIL_HOST_PASSWORD = '$your_smtp_password'
    EMAIL_USE_TLS = True # If your SMTP server requires TLS
    EMAIL_USE_SSL = True # If your SMTP server requires SSL
    CELERY_BROKER_URL = 'amqp://billowconf:$rabbitmq_password@localhost:5672/billowconf'
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'billowconf',
            'USER': '$your_db_user',
            'PASSWORD': '$your_db_password',
            'HOST': 'localhost',
            'PORT': '',
        }
    }

Values starting with ``$`` indicate strings that need replacing with
instance-specific configuration. ``local_settings.py`` is interpreted as
standard Python 3 code, so valid Python is valid here. More settings are
described in the `Django documentation`_. uWSGI is used to serve the BillowConf
to Nginx. The configuration is contained in
``/etc/uwsgi/apps-available/billowconf.ini``::

    [uwsgi]
    env = DJANGO_SETTINGS_MODULE=billowconf.local_settings
    env = NODE_ENV=production
    chdir = $billowconf_install_directory
    module = billowconf.wsgi
    master = true
    plugin = python3
    processes = 10
    vacuum = true

Nginx configuration is contained within
``/etc/nginx/sites-available/billowconf.conf``::

    upstream django {
        server unix:///var/run/uwsgi/app/billowconf/socket;
    }

    server {
        listen 80;
        server_name $BILLOWCONF_URL;

        location / {
            rewrite ^ https://$server_name$request_uri? permanent;
        }
    }

    server {
        listen 443 ssl;
        server_name $BILLOWCONF_URL;
        ssl_certificate /etc/letsencrypt/live/$BILLOWCONF_URL/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/$BILLOWCONF_URL/privkey.pem;
        add_header Strict-Transport-Security "max-age=15552000;";

        ssl_prefer_server_ciphers on;
        ssl_dhparam /etc/dhparams.pem;
        charset utf-8;

        client_max_body_size 75M;

        location /media  {
            alias /$BILLOWCONF_INSTALL_DIR/media;
        }

        location /static {
            alias /$BILLOWCONF_INSTALL_DIR/staticfiles;
        }

        location / {
            uwsgi_pass django;
            include /etc/nginx/uwsgi_params;
        }
    }

Replace all instances of ``$BILLOWCONF_URL`` with the domain name/URL that will
serve BillowConf and all instances of ``$BILLOWCONF_INSTALL_DIR`` with the path
to the BillowConf installation. Enable the configuration as follows::

    sudo ln -s /etc/uwsgi/apps-available/billowconf.ini /etc/uwsgi/apps-enabled/billowconf.ini
    sudo ln -s /etc/nginx/sites-available/billowconf.conf /etc/nginx/sites-enabled/billowconf.conf

Celery configuration requires creating a systemd unit
``/etc/systemd/system/celery.service``::

    [Unit]
    Description=Celery Service
    After=network.target

    [Service]
    Type=simple
    User=www-data
    Group=www-data
    WorkingDirectory=/opt/billowconf
    ExecStart=celery -A billowconf worker --loglevel=INFO

    [Install]
    WantedBy=multi-user.target

Once this file is created, the service unit needs loading into systemd::

    sudo systemctl daemon-reload

Creating the SSL certificate requires LetsEncrypt::

    sudo apt install certbot
    certbot certonly -d $billowconf_url -m $your_email -a standalone --agree-tos

This certificate expires every 90 days and needs to be renewed. This can be done
using the following cron job::

    sudo crontab -e
    * 1 * * * certbot renew --pre-hook 'systemctl stop nginx' --post-hook 'systemctl start nginx'

Once the configuration is complete, BillowConf can be installed and started
using the following script::

    sudo pip3 install -r requirements.txt
    sudo pip3 install mysqlclient
    export DJANGO_SETTINGS_MODULE=billowconf.local_settings
    export NODE_ENV=production
    sudo rabbitmqctl add_user billowconf $rabbitmq_password
    sudo rabbitmqctl add_vhost billowconf
    sudo rabbitmqctl set_permissions -p billowconf billowconf ".*" ".*" ".*"
    sudo mysql
    CREATE USER 'billowconf'@'localhost' IDENTIFIED BY 'billowPass!';
    CREATE DATABASE billowconf;
    GRANT ALL PRIVILEGES ON billowconf.* TO 'billowconf'@'localhost';
    \q
    python3 manage.py collectstatic
    python3 manage.py migrate
    python3 manage.py createsuperuser # Use your email as your username
    mkdir media
    sudo chown www-data:www-data media
    touch billowconf.log
    sudo chown www-data:www-data billowconf.log
    sudo systemctl restart uwsgi
    sudo systemctl restart nginx
    sudo systemctl start celery
    sudo systemctl enable celery

Once all rooms have been configured, and when the Celery workers are restarted,
the room WebRTC streams need to be queued using::

    cd /opt/billowconf
    export DJANGO_SETTINGS_MODULE=billowconf.local_settings
    python3 manage.py streamallrooms

.. _Django documentation: https://docs.djangoproject.com/en/3.0/ref/settings/
