Business Logic
==============

Concepts
--------

* Conference - a virtual collection of people and talks happening at a specific
  time.
* Room - a virtual space where a talk happens and people can engage with it.
* User - a physical person participating in the conference.

  * Anonymous Person - a person who is not authenticated or identified to the
    conference server.
  * Attendee - a person who has authenticated to the conference server.
  * Presenter - an attendee who is currently presenting a talk in a room.
  * Moderator - an attendee who is responsible for moderating the current talk
    in a room. This includes ensuring discussion is within the Code of Conduct,
    the presenter runs on time (for live Talks) and facilitating Q&A.
  * Administrator - an attendee with access permissions to manage the conference
    administration.

* Talk - a scheduled event that 1 or more presenters give. These can be recorded
  or live.

  * Presentation - a traditional talk where 1 or more presenters present while
    people listen. There may be an opportunity for Q&A afterwards.
  * Birds-of-a-Feather (BoF) - a talk where 1 or more presenters facilitate a
    discussion with attendees. The discussion flows between people without a set
    format.

* Schedule - a list mapping talks to rooms with their start and end times.

Implementation
--------------

A conference has 1 or more administrators. These administrators are responsible
for defining the conference schedule, creating rooms and appointing presenters
and moderators. People can then join rooms for talks that they are interested
in. Each talk has 1 or more presenters and 0 or more moderators. The presenter
is responsible for displaying their own slides (OBS, screen sharing, etc). A
talk can be presented live or have recorded content uploaded to play when the
talk is scheduled to start. This allows presenters to be engaged with the
audience while allowing presenters with slower connections to give their talks
without issue. The presenter does not have to be present for recorded talks to
occur. Presenters and moderators can select attendees with questions to ask
their question and manage the queue of questions.

While the back-end server should be independent of the front-end (so that
BillowConf can support both web and native clients), the mocked attendee UI
explains the basic features expected by an attendee watching and interacting
with Talks.

.. figure:: _static/attendee.png
   :alt: Attendee user interface

   This is the mock of up the Attendee view of a room

The moderator/presenter view of a Talk has similar requirements, with the
addition of selecting attendees to ask their questions. Administrators have an
overview of the entire system and can manage the users, talks and conferences.

.. figure:: _static/moderator.png
   :alt: Moderator/Presenter user interface

   This is the mock of up the Moderator/Presenter view of a room

.. figure:: _static/admin_dashboard.png
   :alt: Administrator dashboard of a conference

   This is the mock of up the administrators' overview of a conference

The media exchange happens through WebRTC with a Selective Forwarding
Unit (SFU) routing video between connected clients.
