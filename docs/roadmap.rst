Roadmap
=======

BillowConf expects to develop the following features (the current status is
available here_):

* Integration with conference management systems:

  * Import of a schedule in the `pentabarf format`_.
  * Re-import of a schedule to reflect changes made during the running of the
    conference.
  * User authentication/management integration via OAuth.

* A WebRTC stream for each configured room. Attendees can connect and watch
  presentations and ask questions during Q&A. Birds-of-a-Feather sessions are
  slightly different, and must support a large set of attendees all
  communicating between each other.
* Recording of talks as they happen, dumping the recordings to disk for later
  processing in other applications.
* Support for scheduling recorded content for a talk with the option of a live
  Q&A afterwards
* IRC integration for live discussion for people who do not wish to use
  audio/video or are unable to.
* Support multiple frontends (however, only one, using Vue_, will be developed
  initially).
* Access to rooms can be restricted to ticket holders or be open to anyone.
* Attendees can mute their audio and/or video whenever they choose to do so.
* BillowConf is not an archive system, so it allows the export of data in
  formats that archive systems can use and the import of data from these
  systems.

.. _here: https://gitlab.com/billowconf/billowconf/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=feature%3A%3Aroadmap
.. _pentabarf format: https://debconf19.debconf.org/schedule/pentabarf.xml
.. _Vue: https://vuejs.org/

Future Considerations
---------------------
These things may be a problem or a feature in the future, but the initial design
does not necessarily account for them all. It must, however, be able to grow to
account for them later on. Design decisions now should not prevent BillowConf
from addressing these.

* Bad actors playing copyright material instead of asking a question.
* Integration of other chat clients other than IRC (Mattermost, Slack, Matrix,
  etc) or disabling the chat feature completely.
* Birds-of-a-Feather sessions, where all attendees can interact and converse,
  may need less structured moderation of Attendee interaction with the
  Presenters.
