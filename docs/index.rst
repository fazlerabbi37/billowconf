.. BillowConf documentation master file, created by
   sphinx-quickstart on Mon Mar 23 17:18:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BillowConf
==========

BillowConf is an online platform for virtual conferences. It supports different
rooms that people can join and interact with. Presenters give talks and can
engage with the audience in real time through text (IRC) and video.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   roadmap
   implementation
   install



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
