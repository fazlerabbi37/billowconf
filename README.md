# BillowConf

BillowConf is an online platform for virtual conferences. It supports different
rooms that people can join and interact with. Presenters give talks and can
enagage with the audience in real time through text (IRC) and video.

[Outline](https://writefreely.debian.social/paddatrapper/remote-conference-software)

[Documentation](https://billowconf.gitlab.io/billowconf/)

## Install

```bash
sudo apt install libxml2-dev libxslt-dev libjs-janus libjs-webrtc-adapter rabbitmq-server \
  libavdevice-dev libavfilter-dev libopus-dev libvpx-dev libsrtp2-dev pkg-config npm
virtualenv -p python3 pyenv
pyenv/bin/pip install -r requirements.txt
pyenv/bin/python manage.py migrate
pyenv/bin/python manage.py createsuperuser
sudo rabbitmqctl add_user billowconf password
sudo rabbitmqctl add_vhost billowconf
sudo rabbitmqctl set_permissions -p billowconf billowconf ".*" ".*" ".*"
```

## Run

```bash
cd frontend
npm install -g @vue/cli
npm install
npm run serve
```

In another terminal, start the Django server:

```bash
pyenv/bin/python manage.py runserver
```

This will start a development web server on http://127.0.0.1:8000/

Finally, in another terminal, start the celery server:

```bash
pyenv/bin/celery -A billowconf worker --loglevel=info
```

Room placeholders can also be streamed via the command line. This requires the
room's database ID.

```bash
pyenv/bin/python manage.py streamroom 1
```

## Debian Dependencies

Most of the node dependencies are already in Debian. It currently is missing
vuex and vuetify. The front-end modules need symlinking into
`./frontend/node_modules/` for `npm` to pick them up. This approach is currently
untested.

The Python back-end is missing [aiortc](https://github.com/aiortc/aiortc).

### Back-end

 * python3-django
 * python3-djangorestframework
 * python3-djangorestframework-filter
 * python3-django-webpack-loader
 * python3-celery

### Front-end Production

 * fonts-materialdesignicons-webfont
 * libjs-node-router
 * node-axios
 * node-core-js
 * node-vue-hot-reload-api
 * node-vue
 * node-webrtc-adapter

### Front-end Development
 * node-babel-eslint
 * node-bootstrap-sass
 * node-vue-template-compiler
 * npm
